"use strict";
var __extends = (this && this.__extends) || (function () {
    var extendStatics = Object.setPrototypeOf ||
        ({ __proto__: [] } instanceof Array && function (d, b) { d.__proto__ = b; }) ||
        function (d, b) { for (var p in b) if (b.hasOwnProperty(p)) d[p] = b[p]; };
    return function (d, b) {
        extendStatics(d, b);
        function __() { this.constructor = d; }
        d.prototype = b === null ? Object.create(b) : (__.prototype = b.prototype, new __());
    };
})();
var __awaiter = (this && this.__awaiter) || function (thisArg, _arguments, P, generator) {
    return new (P || (P = Promise))(function (resolve, reject) {
        function fulfilled(value) { try { step(generator.next(value)); } catch (e) { reject(e); } }
        function rejected(value) { try { step(generator["throw"](value)); } catch (e) { reject(e); } }
        function step(result) { result.done ? resolve(result.value) : new P(function (resolve) { resolve(result.value); }).then(fulfilled, rejected); }
        step((generator = generator.apply(thisArg, _arguments || [])).next());
    });
};
var __generator = (this && this.__generator) || function (thisArg, body) {
    var _ = { label: 0, sent: function() { if (t[0] & 1) throw t[1]; return t[1]; }, trys: [], ops: [] }, f, y, t, g;
    return g = { next: verb(0), "throw": verb(1), "return": verb(2) }, typeof Symbol === "function" && (g[Symbol.iterator] = function() { return this; }), g;
    function verb(n) { return function (v) { return step([n, v]); }; }
    function step(op) {
        if (f) throw new TypeError("Generator is already executing.");
        while (_) try {
            if (f = 1, y && (t = op[0] & 2 ? y["return"] : op[0] ? y["throw"] || ((t = y["return"]) && t.call(y), 0) : y.next) && !(t = t.call(y, op[1])).done) return t;
            if (y = 0, t) op = [op[0] & 2, t.value];
            switch (op[0]) {
                case 0: case 1: t = op; break;
                case 4: _.label++; return { value: op[1], done: false };
                case 5: _.label++; y = op[1]; op = [0]; continue;
                case 7: op = _.ops.pop(); _.trys.pop(); continue;
                default:
                    if (!(t = _.trys, t = t.length > 0 && t[t.length - 1]) && (op[0] === 6 || op[0] === 2)) { _ = 0; continue; }
                    if (op[0] === 3 && (!t || (op[1] > t[0] && op[1] < t[3]))) { _.label = op[1]; break; }
                    if (op[0] === 6 && _.label < t[1]) { _.label = t[1]; t = op; break; }
                    if (t && _.label < t[2]) { _.label = t[2]; _.ops.push(op); break; }
                    if (t[2]) _.ops.pop();
                    _.trys.pop(); continue;
            }
            op = body.call(thisArg, _);
        } catch (e) { op = [6, e]; y = 0; } finally { f = t = 0; }
        if (op[0] & 5) throw op[1]; return { value: op[0] ? op[1] : void 0, done: true };
    }
};
Object.defineProperty(exports, "__esModule", { value: true });
require("reflect-metadata");
var express = require("express");
var server_1 = require("./server");
var HTTPMethod;
(function (HTTPMethod) {
    HTTPMethod["GET"] = "get";
    HTTPMethod["POST"] = "post";
    HTTPMethod["PUT"] = "put";
    HTTPMethod["DELETE"] = "delete";
})(HTTPMethod || (HTTPMethod = {}));
function Controller(path, middleware) {
    if (middleware === void 0) { middleware = []; }
    return function (constructor) {
        return /** @class */ (function (_super) {
            __extends(class_1, _super);
            function class_1() {
                var args = [];
                for (var _i = 0; _i < arguments.length; _i++) {
                    args[_i] = arguments[_i];
                }
                var _this = _super.apply(this, args) || this;
                _this.path = path;
                _this.middleware = middleware;
                return _this;
            }
            return class_1;
        }(constructor));
    };
}
exports.Controller = Controller;
function handleError(err, res) {
    var msg = err.message || err.msg || 'SERVER ERROR';
    var status = err.status || 500;
    res.status(status).send({ err: msg });
    return msg;
}
function request(method, url, middleware, disableResponse) {
    if (middleware === void 0) { middleware = []; }
    if (url.substring(0, 1) !== '/')
        url = "/" + url;
    return function (target, propertyKey, descriptor) {
        var initialValue = descriptor.value;
        if (!target['__requests']) {
            target['__requests'] = [];
        }
        target['__requests'].push(propertyKey);
        descriptor.value = function () {
            var _this = this;
            var router = express.Router();
            var groupMiddleware = arguments[0];
            return router[method](url, groupMiddleware.concat(middleware), function (req, res) { return __awaiter(_this, void 0, void 0, function () {
                var logger, r, e_1, msg;
                return __generator(this, function (_a) {
                    switch (_a.label) {
                        case 0:
                            logger = target.logger;
                            if (logger) {
                                logger.info("[" + method.toUpperCase() + "] | " + getFullUrlFromRequest(req) +
                                    (" | PARAMS:" + getParamsFromRequest(req) + " | QUERY:" + getQueryFromRequest(req)) +
                                    (" | BODY: " + getBodyFromRequest(req)));
                            }
                            _a.label = 1;
                        case 1:
                            _a.trys.push([1, 3, , 4]);
                            return [4 /*yield*/, initialValue.call(this, req, res)];
                        case 2:
                            r = _a.sent();
                            if (res.headersSent || disableResponse) {
                                return [2 /*return*/];
                            }
                            if (logger) {
                                logger.silly("[" + method.toUpperCase() + "] | " + getFullUrlFromRequest(req) + " | RESPONSE: " + JSON.stringify(r));
                            }
                            res.send(r);
                            return [3 /*break*/, 4];
                        case 3:
                            e_1 = _a.sent();
                            if (res.headersSent) {
                                return [2 /*return*/];
                            }
                            msg = handleError(e_1, res);
                            try {
                                server_1.Sentry.captureException("[" + method.toUpperCase() + "] | " + getFullUrlFromRequest(req) + " | ERROR: " + msg);
                            }
                            catch (e) {
                            }
                            if (logger) {
                                logger.error("[" + method.toUpperCase() + "] | " + getFullUrlFromRequest(req) + " | ERROR: " + msg);
                            }
                            return [3 /*break*/, 4];
                        case 4: return [2 /*return*/];
                    }
                });
            }); });
        };
        return descriptor;
    };
}
function GetRequest(url, middleware, disableResponse) {
    if (middleware === void 0) { middleware = []; }
    return request(HTTPMethod.GET, url, middleware, disableResponse);
}
exports.GetRequest = GetRequest;
function PostRequest(url, middleware, disableResponse) {
    if (middleware === void 0) { middleware = []; }
    return request(HTTPMethod.POST, url, middleware, disableResponse);
}
exports.PostRequest = PostRequest;
function PutRequest(url, middleware, disableResponse) {
    if (middleware === void 0) { middleware = []; }
    return request(HTTPMethod.PUT, url, middleware, disableResponse);
}
exports.PutRequest = PutRequest;
function DeleteRequest(url, middleware, disableResponse) {
    if (middleware === void 0) { middleware = []; }
    return request(HTTPMethod.DELETE, url, middleware, disableResponse);
}
exports.DeleteRequest = DeleteRequest;
function getFullUrlFromRequest(req) {
    return req.originalUrl;
}
function getParamsFromRequest(req) {
    return JSON.stringify(req.params);
}
function getQueryFromRequest(req) {
    return JSON.stringify(req.query);
}
function getBodyFromRequest(req) {
    try {
        return JSON.stringify(req.body);
    }
    catch (e) {
        return typeof req.body;
    }
}
//# sourceMappingURL=controller.js.map