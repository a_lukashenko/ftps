"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
var __awaiter = (this && this.__awaiter) || function (thisArg, _arguments, P, generator) {
    return new (P || (P = Promise))(function (resolve, reject) {
        function fulfilled(value) { try { step(generator.next(value)); } catch (e) { reject(e); } }
        function rejected(value) { try { step(generator["throw"](value)); } catch (e) { reject(e); } }
        function step(result) { result.done ? resolve(result.value) : new P(function (resolve) { resolve(result.value); }).then(fulfilled, rejected); }
        step((generator = generator.apply(thisArg, _arguments || [])).next());
    });
};
var __generator = (this && this.__generator) || function (thisArg, body) {
    var _ = { label: 0, sent: function() { if (t[0] & 1) throw t[1]; return t[1]; }, trys: [], ops: [] }, f, y, t, g;
    return g = { next: verb(0), "throw": verb(1), "return": verb(2) }, typeof Symbol === "function" && (g[Symbol.iterator] = function() { return this; }), g;
    function verb(n) { return function (v) { return step([n, v]); }; }
    function step(op) {
        if (f) throw new TypeError("Generator is already executing.");
        while (_) try {
            if (f = 1, y && (t = op[0] & 2 ? y["return"] : op[0] ? y["throw"] || ((t = y["return"]) && t.call(y), 0) : y.next) && !(t = t.call(y, op[1])).done) return t;
            if (y = 0, t) op = [op[0] & 2, t.value];
            switch (op[0]) {
                case 0: case 1: t = op; break;
                case 4: _.label++; return { value: op[1], done: false };
                case 5: _.label++; y = op[1]; op = [0]; continue;
                case 7: op = _.ops.pop(); _.trys.pop(); continue;
                default:
                    if (!(t = _.trys, t = t.length > 0 && t[t.length - 1]) && (op[0] === 6 || op[0] === 2)) { _ = 0; continue; }
                    if (op[0] === 3 && (!t || (op[1] > t[0] && op[1] < t[3]))) { _.label = op[1]; break; }
                    if (op[0] === 6 && _.label < t[1]) { _.label = t[1]; t = op; break; }
                    if (t && _.label < t[2]) { _.label = t[2]; _.ops.push(op); break; }
                    if (t[2]) _.ops.pop();
                    _.trys.pop(); continue;
            }
            op = body.call(thisArg, _);
        } catch (e) { op = [6, e]; y = 0; } finally { f = t = 0; }
        if (op[0] & 5) throw op[1]; return { value: op[0] ? op[1] : void 0, done: true };
    }
};
Object.defineProperty(exports, "__esModule", { value: true });
var mongodb_1 = require("mongodb");
var di_1 = require("./di");
var logger_1 = require("./logger");
var MongoService = /** @class */ (function () {
    function MongoService() {
    }
    Object.defineProperty(MongoService.prototype, "client", {
        get: function () {
            return this._client;
        },
        enumerable: true,
        configurable: true
    });
    MongoService.prototype.connect = function (config) {
        var _this = this;
        this.logger.info("CONNECTING TO " + config.url);
        return new Promise(function (resolve, reject) {
            mongodb_1.MongoClient.connect(config.url, { useNewUrlParser: true }, function (err, c) {
                if (err) {
                    _this.logger.error("CONNECTION ERROR | " + err);
                    reject(err);
                }
                else {
                    _this._client = c;
                    _this.logger.info('CONNECTED TO DATABASE');
                    resolve(_this._client);
                }
            });
        });
    };
    MongoService.prototype.collection = function (colName) {
        if (!this._client) {
            this.logger.error('NOT CONNECTED TO DATABASE');
        }
        return this._client.db().collection(colName);
    };
    MongoService.prototype.createCollection = function (colName, options) {
        if (!this._client) {
            this.logger.error('NOT CONNECTED TO DATABASE');
        }
        return this._client.db().createCollection(colName, options || {});
    };
    MongoService.prototype.removeCollection = function (colName) {
        if (!this._client) {
            this.logger.error('NOT CONNECTED TO DATABASE');
        }
        return this._client.db().collection(colName).drop();
    };
    MongoService.prototype.terminate = function () {
        if (!this._client) {
            this.logger.error('NOT CONNECTED TO DATABASE');
        }
        this._client.close();
    };
    __decorate([
        logger_1.Logger('MONGO'),
        __metadata("design:type", Object)
    ], MongoService.prototype, "logger", void 0);
    MongoService = __decorate([
        di_1.Injectable()
    ], MongoService);
    return MongoService;
}());
exports.MongoService = MongoService;
function Transaction() {
    return function (target, propertyKey, descriptor) {
        var initialValue = descriptor.value;
        descriptor.value = function () {
            return __awaiter(this, void 0, void 0, function () {
                var client, mongoService, r, e_1;
                return __generator(this, function (_a) {
                    switch (_a.label) {
                        case 0:
                            mongoService = di_1.Injector.resolve(MongoService);
                            if (mongoService) {
                                client = mongoService.client;
                            }
                            target[propertyKey].client = client;
                            _a.label = 1;
                        case 1:
                            _a.trys.push([1, 3, , 4]);
                            return [4 /*yield*/, initialValue.call(this)];
                        case 2:
                            r = _a.sent();
                            console.log('TA SUCCESS');
                            return [2 /*return*/, r];
                        case 3:
                            e_1 = _a.sent();
                            console.log('TA ERROR');
                            return [2 /*return*/, Promise.reject(e_1)];
                        case 4: return [2 /*return*/];
                    }
                });
            });
        };
        return descriptor;
    };
}
exports.Transaction = Transaction;
//# sourceMappingURL=mongo.js.map