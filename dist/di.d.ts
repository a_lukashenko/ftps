import 'reflect-metadata';
import { Type } from './type';
export interface TypeProvider extends Type<any> {
}
export interface ValueProvider {
    provide: InjectionToken;
    value: any;
}
export declare type Provider = TypeProvider | ValueProvider;
export declare class InjectionToken {
    readonly _key: any;
    constructor(key: string);
    readonly key: string;
}
export declare const SERVER_PROVIDER: InjectionToken;
export declare const APP_PROVIDER: InjectionToken;
export declare const SERVICE_PROVIDER: InjectionToken;
export declare function Inject(token: InjectionToken): (target: Object, propertyKey: string | symbol, parameterIndex: number) => void;
export declare function Injectable(): (target: any) => void;
export declare class Injector {
    static injectorIndexer: number;
    static instances: Map<string, any>;
    static register(target: Provider): void;
    static resolve<T>(target: any): T;
}
