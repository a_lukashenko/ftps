"use strict";
var __extends = (this && this.__extends) || (function () {
    var extendStatics = Object.setPrototypeOf ||
        ({ __proto__: [] } instanceof Array && function (d, b) { d.__proto__ = b; }) ||
        function (d, b) { for (var p in b) if (b.hasOwnProperty(p)) d[p] = b[p]; };
    return function (d, b) {
        extendStatics(d, b);
        function __() { this.constructor = d; }
        d.prototype = b === null ? Object.create(b) : (__.prototype = b.prototype, new __());
    };
})();
Object.defineProperty(exports, "__esModule", { value: true });
var express = require("express");
var bodyParser = require("body-parser");
var cors = require("cors");
var http_1 = require("http");
var fileUpload = require("express-fileupload");
require("reflect-metadata");
var logger_1 = require("./logger");
var di_1 = require("./di");
var SentryLibrary = require("@sentry/node");
var Tracing = require("@sentry/tracing");
function bootstrap(server) {
    return new server();
}
exports.bootstrap = bootstrap;
exports.Sentry = SentryLibrary;
function Server(options) {
    return function (constructor) {
        return /** @class */ (function (_super) {
            __extends(class_1, _super);
            function class_1() {
                var args = [];
                for (var _i = 0; _i < arguments.length; _i++) {
                    args[_i] = arguments[_i];
                }
                var _this = _super.apply(this, args) || this;
                _this.app = express();
                _this.server = http_1.createServer(_this.app);
                _this.run();
                return _this;
            }
            class_1.prototype.run = function () {
                var _this = this;
                if (options.sentryDsn) {
                    exports.Sentry.init({
                        dsn: options.sentryDsn,
                        integrations: [
                            new exports.Sentry.Integrations.Http({ tracing: true }),
                            new Tracing.Integrations.Express({ app: this.app }),
                        ],
                        tracesSampleRate: options.sentryTracesSampleRate || 1.0,
                    });
                }
                this.initProviders([
                    { provide: di_1.APP_PROVIDER, value: this.app },
                    { provide: di_1.SERVER_PROVIDER, value: this.server }
                ].concat(options.providers, [
                    options.initializer
                ]));
                logger_1.LoggerResolver.register('SYSTEM', this, 'logger');
                logger_1.LoggerResolver.resolve(options.loggerConfig);
                var staticDir = options.staticDir;
                if (staticDir) {
                    this.app.use('/static', express.static(staticDir));
                }
                var publicDir = options.publicDir;
                if (publicDir) {
                    this.app.use('/public', express.static(publicDir));
                }
                var bodySize = options.bodySize ? options.bodySize + "mb" : '1mb';
                this.app.use(bodyParser.json({ limit: bodySize }));
                this.app.use(bodyParser.urlencoded({
                    extended: false,
                    limit: bodySize,
                }));
                var whitelistedDomains = options.whitelistedDomains;
                if (options.cors || !whitelistedDomains || !whitelistedDomains.length) {
                    this.app.use(cors());
                }
                else if (whitelistedDomains && whitelistedDomains.length) {
                    var corsOptions = {
                        origin: function (origin, callback) {
                            if (whitelistedDomains.indexOf(origin) !== -1) {
                                callback(null, true);
                            }
                            else {
                                callback(new Error("Domain " + origin + " is not allowed by CORS"));
                            }
                        },
                    };
                    this.app.use(cors(corsOptions));
                }
                this.app.use(fileUpload({ preserveExtension: true, useTempFiles: true }));
                if (options.sentryDsn) {
                    // RequestHandler creates a separate execution context using domains, so that every
                    // transaction/span/breadcrumb is attached to its own Hub instance
                    this.app.use(exports.Sentry.Handlers.requestHandler());
                    // TracingHandler creates a trace for every incoming request
                    this.app.use(exports.Sentry.Handlers.tracingHandler());
                }
                if (options.sentryDsn) {
                    // The error handler must be before any other error middleware and after all controllers
                    this.app.use(exports.Sentry.Handlers.errorHandler());
                }
                this.initControllers(options.controllers, options.prefix);
                this.server.listen(options.port, function () {
                    _this.logger.info("Node Express server listening on http://localhost:" + options.port);
                    var initializer = di_1.Injector.resolve(options.initializer);
                    initializer.init();
                });
            };
            class_1.prototype.initControllers = function (controllers, prefix) {
                var _this = this;
                if (prefix === void 0) { prefix = 'api'; }
                var instances = controllers.map(function (c) {
                    var tokens = Reflect.getMetadata('design:paramtypes', c) || [];
                    var injections = tokens.map(function (token) { return di_1.Injector.resolve(token); });
                    return new (c.bind.apply(c, [void 0].concat(injections)))();
                });
                instances.forEach(function (instance) {
                    var requests = instance['__requests'];
                    if (!requests.length)
                        return;
                    requests.forEach(function (r) {
                        _this.app.use("/" + prefix + instance.path, instance[r](instance.middleware));
                    });
                });
            };
            class_1.prototype.initProviders = function (providers) {
                var valueProviders = providers.filter(function (p) { return typeof p === 'object'; });
                var typeProviders = providers.filter(function (p) { return typeof p === 'function'; });
                valueProviders.forEach(function (provider) {
                    di_1.Injector.register(provider);
                });
                typeProviders.sort(function (p1, p2) {
                    var p1Index = Reflect.getMetadata(di_1.SERVICE_PROVIDER, p1);
                    var p2Index = Reflect.getMetadata(di_1.SERVICE_PROVIDER, p2);
                    if (p1Index > p2Index)
                        return 1;
                    if (p1Index < p2Index)
                        return -1;
                    return 0;
                }).forEach(function (provider) {
                    di_1.Injector.register(provider);
                });
            };
            return class_1;
        }(constructor));
    };
}
exports.Server = Server;
//# sourceMappingURL=server.js.map